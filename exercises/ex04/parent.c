/******************************************************************************
 * File:         display.c
 * Version:      1.4
 * Date:         2018-02-20
 * Author:       M. van der Sluys, J.H.L. Onokiewicz, R.B.A. Elsinghorst, J.G. Rouland
 * Description:  OPS exercise 2: syntax check
 ******************************************************************************/

#include <stdio.h> 
#include <stdlib.h> 
#include "displayFunctions.h" 
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) { 
   
   int i = 0;
   int niceValue = 0, niceIncrementValue; 
   int y = 4;
   ErrCode err;
err = SyntaxCheck(argc, argv);  // Check the command-line parameters
  if(err != NO_ERR) {
    DisplayError(err);        // Print an error message
  } else {
   niceIncrementValue = atoi(argv[3]); 

   
   
   for(i = 0; i < 3; i++){ 
      switch(fork()){ 
         case -1: 
            printf("Fork  failed .\n"); 
            exit(-1); 
            break; 
            
         case 0:
            //child
            if(i == 0)
            {
               nice(niceValue);
               execl("/home/student/exercises/ex02/display","display" ,argv[1], argv[2], argv[y],(char*) NULL);
               perror("execl() failure!\n\n");
               exit(0);
            }
            else
            {
               nice(niceValue);
               niceValue += niceIncrementValue;
               execl("/home/student/exercises/ex02/display","display" ,argv[1], argv[2],argv[y] ,(char*) NULL);
               perror("execl() failure!\n\n");
               exit(0);
            }
            break; 
            
         default: //parent
            printf("parent\n");
            break; 
      }
      y++;
   } 
   for(i=0;i < 3; i++)
   {
      printf("Child %d stopt\n", wait(NULL));
   }
   printf("\n");  // Newline at end
  }
   return 0; 
} 
