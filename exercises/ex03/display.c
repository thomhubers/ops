/******************************************************************************
 * File:         display.c
 * Version:      1.4
 * Date:         2018-02-20
 * Author:       M. van der Sluys, J.H.L. Onokiewicz, R.B.A. Elsinghorst, J.G. Rouland
 * Description:  OPS exercise 2: syntax check
 ******************************************************************************/

#include <stdio.h> 
#include <stdlib.h> 
#include "displayFunctions.h" 
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) { 
  unsigned long int numOfTimes; 
  char printMethod, printChar; 
  ErrCode err;
  pid_t pid;
  int status = 0;
  int i;
  int niceValue = 0, niceIncrementValue; 
  
  err = SyntaxCheck(argc, argv);  // Check the command-line parameters 
  if(err != NO_ERR) { 
    DisplayError(err);        // Print an error message 
  } else {     
    printMethod = argv[1][0]; 
    numOfTimes = strtoul(argv[2], NULL, 10);  // String to unsigned long 
     
    niceIncrementValue = atoi(argv[3]); 
     
    printChar = argv[4][0]; 
    nice(niceValue); 
    PrintCharacters(printMethod, numOfTimes, printChar);  // Print character printChar numOfTimes times using method printMethod
    pid = fork();

    for(i = 0; i < 2; i++){ 
      niceValue += niceIncrementValue; 
      switch(pid){ 
      case -1: 
    printf("Fork  failed .\n"); 
    exit(-1); 
    break; 
      case 0:
    //child 
    printChar = argv[5 + i][0];
    nice(niceValue);  
    PrintCharacters(printMethod, numOfTimes, printChar);  // Print character printChar numOfTimes times using method printMethod
    
    exit(0); 
    break; 
      default: //parent
	pid = wait(&status);
    printf("parent");
    break; 
      } 
    } 
  } 
  printf("\n");  // Newline at end 
  return 0; 
} 

