#include <iostream>
#include <cstring>

using namespace std;

int main()
{
   string str1 {"Hallo"};
   string str2 {"Wereld"};
   string str3;
   cout << "String 1 = " << str1 << endl;
   cout << "String 2 = " << str2 << endl;

   str2 = str1;
   cout << "Str2 = str1 = " << str2 << endl;
   str3 = str1 + str2;
   cout << "str1 + str2 = string 3 = " << str3 << endl;
   str3.clear();
   cout << "string 3 empty = " << str3 << endl;
   str3.insert(0,str1);
   cout << "string 1 insert in empty string 3 = " << str3 << endl;
   cout << endl;
   // clear strings
   str1.clear();
   str2.clear();
   str3.clear();
   cout << "All string are cleared" << endl;
   str1 = {"Groene tesla"};
   str2 = {"Zwarte tesla"};
   if (str1.compare(str2) !=0)
   {
      cout << str1 << " is geen " << str2 << endl;
   }
   if (str1.compare(7,5, str2,7,5) == 0)
   {
      cout << "beide zijn tesla's" << endl;
   }
   else
   {
      cout << "het zijn verschillende automerken" << endl;
   }
   return 0;
}
